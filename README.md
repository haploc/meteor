# README #


Meteor is an open source HTTP server, designed to offer developers a simple means of integrating streaming data into web applications without the need for page refreshes.

This repository is for a quick setup with Vagrant + ansible provisioning.

Vagrant image: currently used is "chef/centos-6.5" 

Network settings: After staging, it will listen on IP 192.168.1.98 on my bridge br0 interface (check Vagrantfile for further details)